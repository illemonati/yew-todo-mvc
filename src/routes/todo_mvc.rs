use wasm_bindgen::prelude::*;
use yew::prelude::*;

/// TodoMVC page
pub struct TodoMVC {
    link: ComponentLink<Self>,
    todoInputValue: String,
    todos: Vec<String>,
}

pub enum Msg {
    TodoInputValueChange(String),
    NewTodo,
    DeleteTodo(usize),
}

impl TodoMVC {
    fn renderTodo(&self, todo: &str, index: usize) -> Html {
        html! {
            <div class="todoListItem">
                <div class="todoListItemText">
                    {format!("{}. {}", index, todo)}
                </div>
                <div class="todoListItemDeleteButton"
                        onclick=self.link.callback(move |_| Msg::DeleteTodo(index))
                >
                    {"X"}
                </div>
            </div>
        }
    }
}

impl Component for TodoMVC {
    type Message = Msg;
    type Properties = ();

    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        TodoMVC {
            link,
            todoInputValue: String::new(),
            todos: Vec::new(),
        }
    }

    fn change(&mut self, _: Self::Properties) -> ShouldRender {
        false
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Self::Message::TodoInputValueChange(new_val) => {
                self.todoInputValue = new_val;
            }
            Self::Message::NewTodo => {
                if self.todoInputValue.len() > 0 {
                    self.todos.push(self.todoInputValue.clone());
                    self.todoInputValue = String::new();
                }
            }
            Self::Message::DeleteTodo(index) => {
                self.todos.remove(index);
            }
        }
        true
    }

    fn view(&self) -> Html {
        html! {
            <div class="todoMVC">
                <h1 class="title"> { "Yew Todo MVC" } </h1>
                <br />
                <br />
                <br />
                <br />
                <div class="inputBox">
                    <input class="mainTodoInput"
                        type="text"
                        value=self.todoInputValue
                        oninput=self.link.callback(|e: InputData| Msg::TodoInputValueChange(e.value))
                    />
                    <div class="newTodoButton"
                            onclick=self.link.callback(|_| Msg::NewTodo)
                    > {"+"} </div>
                </div>
                <br />
                <br />
                <div class="todoList">
                    {self.todos.iter().enumerate().map(|(i, x)| self.renderTodo(x, i)).collect::<Html>()}
                </div>
            </div>
        }
    }
}
